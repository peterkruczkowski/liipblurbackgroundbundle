<?php

namespace PeterKruczkowski\LiipBlurBackgroundBundle\Imagine\Filter\Loader;

use Imagine\Image\Box;
use Imagine\Image\ImageInterface;
use Imagine\Image\Point;
use Liip\ImagineBundle\Imagine\Filter\Loader\LoaderInterface;

class BlurBackgroundLoader implements LoaderInterface
{
    public function load(ImageInterface $image, array $options = []): ImageInterface
    {
        list($radius, $sigma) = $options['radius_sigma'];
        list($width, $height) = $options['size'];

        $size = $image->getSize();

        if ($size->getHeight() >= $size->getWidth()) {

            $background = $image->copy();
            $background->resize(new Box($width, $height));
            $background->getImagick()->blurImage($radius, $sigma);

            $ratio = $height / $size->getHeight();
            $image->resize(new Box($size->getWidth() * $ratio, $height));

            $x = ($width - $image->getSize()->getWidth()) / 2;
            $y = ($height - $image->getSize()->getHeight()) / 2;

            $background->paste($image, new Point($x, $y));

            $image = $background;
        } else {
            $ratio =  $width / $size->getWidth();
            $image->resize(new Box($width, $size->getHeight() * $ratio));
            $y = ($height - $image->getSize()->getHeight()) / 2;
            $image->crop(new Point(0, abs($y)), new Box($width, $height));
        }

        return $image;
    }
}
