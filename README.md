# LiipBlurBackgroundBundle

Symfony bundle with filter for LiipImagine to set a blur background.

Example usage with LiipImagine:

```yaml
liip_imagine:
  driver: "imagick"
  filter_sets:
    cache: ~
    blur_image:
      jpeg_quality:          75
      png_compression_level: 7
      filters:
        blur_background:
          size: [960, 540]
          radius_sigma: [50, 10]
```
